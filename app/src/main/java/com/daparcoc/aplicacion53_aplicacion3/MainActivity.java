package com.daparcoc.aplicacion53_aplicacion3;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {

    private static final int MNU_OPC1 = 1;
    private static final int MNU_OPC2 = 2;
    private static final int MNU_OPC3 = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        switch(item.getItemId()) {
            case R.id.action_acercade:
                builder.setTitle("Acerca de ...");
                builder.setMessage("Nombre: David Aparco Cárdenas\n" +
                        "Código: 20110308D");
                builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
                builder.show();
                break;
            case R.id.action_configuracion:
                Intent intent = new Intent(MainActivity.this, SegundaActividad.class);
                startActivity(intent);
                break;
            case R.id.action_salir:
                finish();
            default:
        }

        return super.onOptionsItemSelected(item);
    }
}
